#!/bin/bash
export _condor_CONDOR_HOST=tweetybird03.cern.ch
export _condor_COLLECTOR_HOST=tweetybird03.cern.ch
export _condor_SCHEDD_HOST=bigbird26.cern.ch
export _condor_SCHEDD_NAME=bigbird26.cern.ch
export _condor_SEC_CLIENT_AUTHENTICATION_METHODS=KERBEROS
export _condor_SEC_CREDENTIAL_PRODUCER=/app/batch_krb5_credential
export _condor_CREDD_HOST=bigbird26.cern.ch
export _condor_FILESYSTEM_DOMAIN=cern.ch
export _condor_UID_DOMAIN=cern.ch
export _condor_NETWORK_INTERFACE=*
export KRB5CCNAME=/run/user/$UID/krb5cc
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh

exec "$@"
