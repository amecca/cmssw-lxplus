#!/usr/bin/perl -w -T
#------------------------------------------------------------
#
# CERN IT, DES
#
# File        : batch_krb5_credential
# Description : Creates AP_REQ message for authenticating expired/expiring tgt renewals for batch jobs.
# Author      : Zakarias Juto (With some legacy code by Rainer Toebbicke)
#
#------------------------------------------------------------

###################################
#          Initial Setup          #
###################################

use Authen::Krb5 (ADDRTYPE_INET,ADDRTYPE_IPPORT);
use strict;
use IPC::Open2;
use IO::File;
use Getopt::Long qw(GetOptions);
use Sys::Syslog;
use Sys::Syslog qw(:standard :macros);
use File::Temp qw/ tempfile tempdir /;
use Sys::Hostname;
use Archive::Tar;
use File::Copy;

$SIG{INT}=sub{die};
$ENV{PATH} = "/usr/bin";
$ENV{LANG} = 'C';

$ENV{RANDFILE} = "/dev/null"; # openssl random seed file, avoids "unable to write 'random state'"
$ENV{KRB5_CONFIG} = "/etc/krb5.conf"; # get tickets for unresolved service name
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'}; # various things that make taint mode unhappy

my $configfile = "/etc/sysconfig/ngbauth-submit";

my $principalName = "ngauth/ngass.cern.ch";
my $serviceName;
my $serverName;
my $pubkey = "/orig/etc/ngauth_batch_crypt_pub.pem";
my $debug = 0 ;
my $needhelp;

my $openssl = "/usr/bin/openssl";
my $cmd;


Authen::Krb5::init_context();

my $authContext = new Authen::Krb5::AuthContext;
my $credCache = Authen::Krb5::cc_default();

sub Usage () {
    print <<EOFusage;
$0: [--debug] [--principal SERVICE] [--pubkey PUBKEY]

Create opaque batch token credential from a kerberos service ticket
for SERVICE (Krb5 principal, no realm), encrypt it with PUBKEY and
print (base64) to STDOUT.

Defaults / current config:
  SERVICE=$principalName
  PUBKEY=$pubkey
EOFusage
}

# Attempts to create and return an APREQ message.
sub createMessage () {
  if($debug) {
	   my $principal = $credCache->get_principal();
	   my $p_type = $principal->type();
	   my $user = $principal->data();
	   my $realm =  $principal->realm();

     &Log("DEBUG: Using credential cache ".$credCache->get_name()." for ".$p_type.':'.$user.'@'.$realm.".") if ($debug);
    }
    # Use the credential cache and the auth context to create an AP_REQ message for the server.
    &Log("DEBUG: Requesting AP_REQ for $serviceName/$serverName") if ($debug);
    my $in_data = $>.':'.$ENV{'USER'}.':'.hostname().':'.time();  # this is signed together with message
    # The super server information. Needs to be filled in with the actual service name and host name.
    my $apreq = Authen::Krb5::mk_req($authContext, 0, $serviceName, $serverName, $in_data, $credCache);
    &Die("ERROR: No AP_REQ message created by kerberos: " . Authen::Krb5::error()) unless $apreq;
    &Log("DEBUG: AP_REQ created successfully.") if ($debug);

    return $apreq;
}


# Logging function that takes input and logs it with syslog.
sub Log ( $ ){
    my ($info) = @_;
    if (! $debug) {
	openlog(my $program, 'cons,pid,nofatal', 'user');
	syslog('info', '%s', $info);
	closelog();
    } else {
	print STDERR "$info\n";
    }
}


# Replaces regular die by also checking if we should log something.
sub Die {
    if (! $debug ) { # do not log twice on exit
	&Log("@_\n");
    }
    die "@_\n";
}


# Legacy function that takes a command and runs it on a specific input. Leaving it intact as it is incredibly handy for all the openssl calls.
sub RUN2 ( $$$ ) {
    my($cmd, $what, $input) = @_;
    my $l;
    my $pid = open2(\*FROM_P, \*TO_P, $cmd) || &Die("ERROR (RUN2): Cannot open pipe to $what");
    if ($input) {
	$l = syswrite(TO_P, $input);
	&Log( "DEBUG (RUN2): wrote $l bytes to $what...") if ($debug > 1);
    }
    close(TO_P);

    my $offset = 0;
    my $result = "";
    while ($l = sysread(FROM_P, $result, 4*4096, $offset)) {
	$offset += $l;
	&Log( "DEBUG (RUN2): read $l bytes from $what...") if ($debug > 1);
    }
    close(FROM_P);
    waitpid( $pid, 0 );
    my $child_exit_status = $? >> 8;
    my($CMD) = $cmd =~ m'[^\s]*/([^/\s]+)';
    &Die("ERROR (RUN2): Cannot run $CMD to $what:$child_exit_status: $!") if ($child_exit_status);
    &Die("ERROR (RUN2): $CMD failed to $what (\$result undef)") unless ($result);

    return $result;
}

# Parser for the configuration file.
sub getConf () {
    if (! open FILE, "$configfile") {
	&Log("Couldn't open configuration file $configfile: $!");
	return;
    }
    my @conflines = <FILE>;
    close FILE;
    foreach my $confline (@conflines){
	chomp($confline);
	next if ($confline =~ /^\s*#/);  # skip comments
	next if ($confline =~ /^\s*$/);  # and skip empty lines..
        if ($confline =~ m/^principalname="?([[:alnum:]._\/-]+)"?$/){
            $principalName = $1;
        } elsif ($confline =~ /^pubkey="?([[:alnum:]._\/-]+)"?$/){
            $pubkey = $1;
        }  else {
	    &Log("WARN: ignoring unknown config line '$confline'");
	}
    }
}

###### arguments and cross-checks
#&getConf();
if (!GetOptions(
	 'd|debug' => \$debug,
	 'h|help' => \$needhelp,
	 'principal=s' => \$principalName,
	 'pubkey=s' => \$pubkey,
    )) {
    &Usage();
    &Die("ERROR: bad arguments");
}
# untaint args (file and command line)
if( $principalName =~ m/^([[:alnum:]]+)\/([[:alnum:].-]+)$/) {
    $serviceName = $1;
    $serverName = $2;
} else {
    &Die("ERROR: bad service name");
}
if( $pubkey =~ m/^([[:alnum:]._\/-]+)$/) {
    $pubkey = $1;
} else {
    &Die("ERROR: bad pubkey file");
}

if ($needhelp) {
    &Usage();
    exit(0);
}
if ($debug){
    &Log ("Debug mode active.");
}
&Log("Service Name set to: $serviceName") if ($debug);
&Log("Server Name set to: $serverName") if ($debug);
&Log("Public key should be found in: '$pubkey'") if ($debug);

unless (-e $pubkey) {
    &Die("ERROR (Initial Setup): No access to public key in '$pubkey'.");
}

###################################
#            Execution            #
###################################

# Create the AP_REQ message.
my $message = &createMessage();

# Get the length of the apreq message. (To 6 digits).
my $messageLength = sprintf("%06d", length($message));
&Log("DEBUG: Length of the created apreq message: $messageLength") if ($debug);

# Create composite from the apreq message and it's length.
my $composite = $messageLength . $message;

# Generate a symmetric key.
my ($fh, $symkeyfilename) = tempfile(UNLINK => !($debug));
system($openssl,"rand","-out","$symkeyfilename","-base64","64") == 0 or
    &Die("ERROR (openssl): Failed to generate random symmetric key.");
&Log("DEBUG: Random symmetric key generated into $symkeyfilename.") if ($debug);


# Use the symmetric key to encrypt the composite.
my $redirect = $debug ? "" : "2>/dev/null";
$cmd = "$openssl enc -aes-256-cbc -md md5 -salt -pass 'file:$symkeyfilename' ".$redirect;
my $encComposite = RUN2($cmd, "encrypt data", $composite);
&Die("ERROR: Failed to encrypt composite.") unless $encComposite;
&Log("DEBUG: Salted composite successfully encrypted, length=".(length($encComposite)).".") if ($debug);


# Now encrypt the symmetric key with the batch computer public key.
$cmd = "$openssl pkeyutl -inkey $pubkey -pubin -encrypt -in '$symkeyfilename'";
my $encSymkey = RUN2($cmd, "encrypt symmetric key", undef);
&Die("ERROR: Failed to encrypt the symmetric key.") unless $encSymkey;
# Get the fixed=padded length of the encrypted symmetric key.
my $keyLength = sprintf("%06d", length($encSymkey));
&Log("DEBUG: Symmetric key encrypted ($keyLength bytes) with key from: $pubkey") if ($debug);
# Prefix the key length and the encrypted symmetric key to the composite, add a header and some separators.
my $finalComposite = 'NGAUTH'.':'.$keyLength .':'. $encSymkey .':'. $encComposite;


# Encode to base64.
# FIXME: why not MIME::Base64::encode_base64 ?
$cmd = "$openssl enc -e -base64 -A";
my $encoded = RUN2($cmd, "encode string", $finalComposite);
&Die("ERROR: Failed to encode composite to base 64") unless $encoded;
&Log("DEBUG: Composite encoded to base 64.") if ($debug);

# Print to stdout (LSF/Condor in charge of making sure it is at the batch machine at job start.)
my $ngauth_apreq = "-----BEGIN NGAUTH APREQ-----\n".$encoded."\n-----END NGAUTH APREQ-----\n";
my $tgt = $credCache->get_name();
my ($tgt_fh, $tgt_fn) = tempfile();


my $newCreds = Authen::Krb5::cc_resolve("FILE:".$tgt_fn);
$newCreds->initialize($credCache->get_principal());
#Authen::Krb5::cc_copy_creds($credCache, $newCreds);
copy($credCache, $newCreds);

$ENV{'KRB5CCNAME'} = "FILE:".$tgt_fn;
# try to kinit -R to get rid of any service tokens for privacy's sake
system("/usr/bin/kinit -R > /dev/null 2>&1");
# tar / b64 the ap_req and the tgt
my $tar = Archive::Tar->new;
$tar->add_files($tgt_fn);
substr($tgt_fn, 0, 1) = "";
$tar->rename($tgt_fn, $ENV{'USER'}.".cc.submit");
$tar->add_data($ENV{'USER'}.".apreq", $ngauth_apreq);
if ($tar->can('chown')) {
  $tar->chown($ENV{'USER'}.".apreq", "root", "root");
  $tar->chown($ENV{'USER'}.".cc.submit", "root", "root");
}
my ($tfh, $tg_fn) = tempfile(UNLINK => !($debug));
$tar->write($tg_fn);
my $b64_tarred = `$openssl enc -e -base64 -in $tg_fn`;

print STDOUT "-----BEGIN NGAUTH COMPOSITE-----\n".$b64_tarred."\n-----END NGAUTH COMPOSITE-----\n";

&Log("DEBUG: Composite dumped to stdout.") if ($debug);

# Quit, we're all done.
Authen::Krb5::free_context();
exit 0;
